<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Our Product</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/simple.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
</head>
<body>
    <section class="container-fluid">
        <div class="row">
            <div class="col-lg-3 sidemenu text-center">
                <h1 class="mt-5 white-text">Grand Product</h1>
            </div>
            <div class="col-lg-9">
                <div class="row justify-content-center">
                    <div class="col-10 mt-5">
                    
                    <h2>Product List</h2>
                    <p class="lead">jumlah produk dalam setiap kategori</p>
                    <table border="1" class="table table-bordered" id="table_product">
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>Jumlah_product</th>
                        </tr>
                        <?php

                        //connection
                        $conn = new mysqli("localhost" , "root" , "config123" , "db_product");


                        $sql = "SELECT products.id as id, product_categories.name , COUNT(*) as Jumlah_Product FROM `products` JOIN product_categories ON products.category_id=product_categories.id GROUP BY category_id";

                        $result = $conn->query($sql);

                        if ($result->num_rows > 0) {
                            while ($result_rows = $result->fetch_object()) {

                        ?>
                            <tr>
                                <td><?= $result_rows->id ?></td>
                                <td><?= $result_rows->name ?></td>
                                <td><?= $result_rows->Jumlah_Product ?></td>
                            </tr>

                        <?php 
                            }
                        }
                        
                        ?>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>