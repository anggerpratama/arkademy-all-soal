<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data Result</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .tables{
            border-collapse: collapse;
        }
        .tables th{
            padding: 10px 20px;
        }
        .tables td{
            padding: 10px 20px;
        }
    </style>
</head>
<body>
    <table border="1" class="tables">
        <tr>
            <th>id</th>
            <th>name</th>
            <th>Jumlah_product</th>
        </tr>
        <?php 

        //connection
        $conn = new mysqli("localhost" , "root" , "config123" , "db_product");


        $sql = "SELECT products.id as id, product_categories.name , COUNT(*) as Jumlah_Product FROM `products` JOIN product_categories ON products.category_id=product_categories.id GROUP BY category_id";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while ($result_rows = $result->fetch_object()) {

        ?>
            <tr>
                <td><?= $result_rows->id ?></td>
                <td><?= $result_rows->name ?></td>
                <td><?= $result_rows->Jumlah_Product ?></td>
            </tr>

        <?php 
            }
        }
        
        ?>
    </table>
</body>
</html>