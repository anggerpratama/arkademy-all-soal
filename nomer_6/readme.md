# Jawaban Soal 6
saya disini menggunakan bahasa pemrograman PHP. Jadi untuk menjalankan file ini yaitu copy folder ini atau clone dari gitlab. setelah itu taruh di `xampp/htdocs` dan run webserver xamppnya. Lalu kunjungi localhost/namafolder. juga ada file .sql untuk mengimport database

### Query SQL
ini dalah script sql untuk menampilkan data seperti dalam soal

```sql
SELECT products.id as id, product_categories.name as name , COUNT(*) as Jumlah_Product FROM `products` JOIN product_categories ON products.category_id=product_categories.id GROUP BY category_id
```

dan ini adalah hasilnya

<img src="result.PNG">