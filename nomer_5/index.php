<?php 

echo characterCount('soleram solore' , 'o');

function characterCount($text , $char)
{
    $total = 0;
    $split = str_split($text);
    $split_limit = count($split);

    for ($i=0; $i < $split_limit; $i++) { 
        if ($split[$i] == $char) {
            $total += 1;
        }
    }

    return "Jumlah Huruf '".$char."' Dari Kata '".$text."' Adalah ".$total;

}


?>