<?php 

    
    function productJson()
    {
        $product = array(
            'itemId' => "12341822",
            'itemName' => 'basic t-shirt',
            'price' => 70000,
            'availableColorAndSize' => array(
                0 => array('color' => 'red', 'size' => 'S , M , L'),
                1 => array('color' => 'solid black' , 'size' => 'M , L'),
            ),
            'freeShiping' => false,
        );
    
        return json_encode($product);
    }

    $jsonProduct = productJson();

    echo $jsonProduct;


    

?>